-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 16, 2017 at 07:50 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.5.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pis_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `doctor`
--

CREATE TABLE `doctor` (
  `id` int(11) NOT NULL,
  `firstname` varchar(15) NOT NULL,
  `middlename` varchar(15) NOT NULL,
  `lastname` varchar(15) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(32) NOT NULL,
  `age` int(11) NOT NULL,
  `gender` enum('Male','Female','Others','') NOT NULL,
  `address` varchar(30) NOT NULL,
  `qualification` varchar(25) DEFAULT NULL,
  `contact_no` text NOT NULL,
  `department` enum('Ear, Nose and Throat','Gastroenterology','Ophthalmology','') NOT NULL,
  `joined_date` varchar(25) DEFAULT NULL,
  `remarks` text,
  `created_at` datetime DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `postby_id` int(10) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updatedby_id` int(10) DEFAULT NULL,
  `status` int(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doctor`
--

INSERT INTO `doctor` (`id`, `firstname`, `middlename`, `lastname`, `username`, `password`, `age`, `gender`, `address`, `qualification`, `contact_no`, `department`, `joined_date`, `remarks`, `created_at`, `postby_id`, `updated_at`, `updatedby_id`, `status`) VALUES
(1, 'Ram', 'Kumar', 'Shrestha', 'ram123', '6a557ed1005dddd940595b8fc6ed47b2', 30, 'Male', 'Balaju', 'MBBS', '9841238790', 'Ophthalmology', '2015-09-12', NULL, '2017-10-15 16:26:57', NULL, NULL, NULL, 1),
(2, 'Sima', 'Kumari', 'Ghimire', 'sima', 'e6b42073f30a539405c50c443633c160', 29, 'Female', 'Kathmandu', 'MMBS', '9813247898', 'Gastroenterology', '2016--2-28', NULL, '2017-10-15 11:05:03', NULL, NULL, NULL, 1),
(3, 'Binod', '', 'Shrestha', 'binod', '1f49756f262ef0b16355bb23eff4f828', 40, 'Male', 'Kirtipur', 'MBBS', '9843167567', 'Ear, Nose and Throat', '2016-04-11', NULL, '2017-10-15 11:06:17', NULL, NULL, NULL, 1),
(4, 'Nirmala', '', 'Shahi', 'nirmala', '1b1d167f4ad4c8dbf6f9f7535ec2403a', 33, 'Female', 'Sitapaila', 'MBBS', '9844123758', 'Ophthalmology', '2017-03-19', NULL, '2017-10-15 11:53:12', NULL, NULL, NULL, 0),
(5, 'Salina', '', 'Dhakal', 'salina', 'fea2f233d65f0bd3a62059c63b324cde', 32, 'Female', 'Dhulikhel', 'MBBS', '9813252636', 'Ophthalmology', '2016-08-13', NULL, '2017-10-15 11:03:08', NULL, NULL, NULL, 1),
(6, 'Rushab', '', 'Lama', 'rushab', 'f0500d8d45a3ffa50e5c2eb7824a87c8', 40, 'Male', 'Bhaktapur', 'MBBS', '9841237583', 'Ophthalmology', '2016-03-15', NULL, '2017-10-15 22:22:41', NULL, NULL, NULL, 1),
(7, 'Hari', 'Krishna', 'Dahal', 'hari', 'a9bcf1e4d7b95a22e2975c812d938889', 30, 'Male', 'Kupondole', 'MBBS', '9841238764', 'Ear, Nose and Throat', '2016-07-23', NULL, '2017-10-15 11:08:27', NULL, NULL, NULL, 1),
(8, 'Rekha', '', 'Thapa Magar', 'rekha', '056062863ed19a32787a3ae9e1117d5e', 28, 'Female', 'Lalitpur', 'MBBS', '9841238764', 'Gastroenterology', '2016-03-28', NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, 1),
(9, 'Binita', '', 'Bajagain', 'binita', '66ec78c9658bfbc52708a456656c77b6', 45, 'Female', 'Maitighar', 'MBBS', '9851754839', 'Ear, Nose and Throat', '2014-05-04', NULL, '2017-10-15 11:16:33', NULL, NULL, NULL, 0),
(10, 'Dinesh', '', 'Humagain', 'dinesh', '9c9f1c65b1dc1f79498c9f09eb610e1a', 37, 'Male', 'Banasthali', 'MBBS', '9841674589', 'Gastroenterology', '2015-08-13', NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, 1),
(11, 'Lakpa', '', 'Sherpa', 'lakpa', 'd55865fe9b8984a878e25ea6f8e77f10', 34, 'Male', 'Kavrepalanchowk', 'MBBS', '9843164859', 'Ear, Nose and Throat', '2017-10-15', NULL, '2017-10-15 11:25:23', NULL, NULL, NULL, 0),
(12, 'Sarita', '', 'Maharjan', 'sarita', 'fb4e529ea6b9320c8bd32577f78a7fdf', 31, 'Female', 'Sundhara', 'MBBS', '9841764839', 'Ophthalmology', '2015-09-17', NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, 1),
(13, 'Shyam', '', 'Gautam', 'shyam', '5a4cd850fc787d454416aa3a47580468', 40, 'Male', 'Koteshor', 'MBBS', '9841786435', 'Ear, Nose and Throat', '2017-12-29', NULL, '2017-10-15 11:38:49', NULL, NULL, NULL, 0),
(14, 'Mina', '', 'Tiwari', 'mina', 'b90ea060fb297c28f8b300a15cd15124', 33, 'Female', 'Gathaghar', 'MBBS', '9841876453', 'Ear, Nose and Throat', '2015-04-30', NULL, '2017-10-16 08:04:15', NULL, NULL, NULL, 1),
(15, 'Sabina', '', 'Manandhar', 'sabina', 'eb3eeacd08783aec919e447f3b4e2499', 28, 'Female', 'Bhaktapur', 'MBBs', '9841683947', 'Ophthalmology', '2016-06-21', NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, 1),
(16, 'Mashoor', '', 'Gulati', 'mashoor', 'b0e978513c08153291faba940c961c13', 30, 'Male', 'Lokanthali', 'MBBS', '9849463728', 'Ear, Nose and Throat', '2016-03-22', NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `patient`
--

CREATE TABLE `patient` (
  `id` int(11) NOT NULL,
  `firstname` varchar(15) NOT NULL,
  `middlename` varchar(15) NOT NULL,
  `lastname` varchar(15) NOT NULL,
  `age` int(11) DEFAULT NULL,
  `gender` enum('Male','Female','Others','') NOT NULL,
  `address` varchar(30) NOT NULL,
  `relative` varchar(25) DEFAULT NULL,
  `contact_no` text NOT NULL,
  `height` varchar(25) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `BP` varchar(25) NOT NULL,
  `department` enum('Ear, Nose and Throat','Gastroenterology','Ophthalmology','') NOT NULL,
  `consultant` varchar(25) NOT NULL,
  `remarks` text,
  `created_at` datetime DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `postby_id` int(10) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updatedby_id` int(10) DEFAULT NULL,
  `status` int(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `patient`
--

INSERT INTO `patient` (`id`, `firstname`, `middlename`, `lastname`, `age`, `gender`, `address`, `relative`, `contact_no`, `height`, `weight`, `BP`, `department`, `consultant`, `remarks`, `created_at`, `postby_id`, `updated_at`, `updatedby_id`, `status`) VALUES
(1, 'Rima', '', 'Timalsina', 25, 'Female', 'Koteshor,Ktm', 'Own', '9813748393', '5ft 5inch', 55, '120mm/Hg', 'Ophthalmology', 'Dr. Nirmala Shahi', NULL, '2017-10-15 16:31:28', NULL, NULL, NULL, 1),
(2, 'Nabina', '', 'Manandhar', 20, 'Female', 'Banepa', 'Mother', '9841198765', '5ft', 45, '110mm/Hg', 'Ear, Nose and Throat', 'Dr. Binod Shrestha', NULL, '2017-10-15 16:29:29', NULL, NULL, NULL, 1),
(3, 'Shree', 'Bahadur', 'Karki', 50, 'Male', 'Kalimati', 'Wife', '9849874609', '5ft 6inch', 65, '110mm/Hg', 'Gastroenterology', 'Dr. Dinesh Humagain', NULL, '2017-10-15 16:31:08', NULL, NULL, NULL, 1),
(4, 'Dharma', 'Ratna', 'Shakya', 62, 'Male', 'Bijulinagar', 'Daughter', '9841794093', '6ft 2inch', 70, '120mm/Hg', 'Gastroenterology', 'Dr. Dinesh Humagain', NULL, '2017-10-15 16:30:37', NULL, NULL, NULL, 1),
(5, 'Nikita', '', 'Subedi', 23, 'Female', 'Charikot', 'Mother', '9841758374', '5ft 7inch', 55, '120', 'Ear, Nose and Throat', 'Dr. Binita Bajagain', NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, 1),
(6, 'Hom', 'Bahadur', 'Tamang', 55, 'Male', 'Jorpati', 'Own', '9843674980', '5ft 4inch', 60, '120mm/Hg', 'Gastroenterology', 'Dr. Rekha Thapa Magar', NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, 1),
(7, 'Buddhi', 'Ram', 'Dhakal', 70, 'Male', 'Patan', 'Son', '9843759830', '5ft 9inch', 75, '120mm/Hg', 'Gastroenterology', 'Dr. Sima Kumari Ghimire', NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, 1),
(8, 'Suman', '', 'Guragain', 25, 'Male', 'Biratnagar', 'Own', '9841245644', '6ft 1inch', 66, '120mm/Hg', 'Ophthalmology', 'Dr. Rushab Lama', NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, 1),
(9, 'Champa', '', 'Manandhar', 63, 'Female', 'Sanga', 'Husband', '9841345088', '5ft 1inch', 48, '110mm/Hg', 'Gastroenterology', 'Dr. Sima Kumari Ghimire', NULL, '2017-10-16 01:35:58', NULL, NULL, NULL, 0),
(10, 'Rashid', 'Ali', 'Khan', 40, 'Male', 'Basantapur', 'Wife', '9813679899', '6ft 3inch', 67, '110mm/Hg', 'Ear, Nose and Throat', 'Dr. Lakpa Sherpa', NULL, '2017-10-16 01:35:43', NULL, NULL, NULL, 1),
(11, 'Prem', '', 'Maharjan', 76, 'Male', 'Baneshor', 'Son', '9841758490', '6ft 2inch', 66, '120mm/Hg', 'Ear, Nose and Throat', 'Dr. Hari Krishna Dahal', NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, 1),
(12, 'Suresh', '', 'Shrestha', 38, 'Male', 'Kupondole', 'Own', '9841298493', '6ft', 60, '120', 'Ear, Nose and Throat', 'Dr. Mashoor Gulati', NULL, '2017-10-15 14:11:37', NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `fullname` varchar(30) NOT NULL,
  `username` varchar(30) NOT NULL DEFAULT '',
  `email` varchar(40) DEFAULT NULL,
  `contact_no` text,
  `password` varchar(32) NOT NULL DEFAULT '',
  `user_type` enum('Admin','Doctor','','') NOT NULL DEFAULT 'Admin',
  `address` varchar(30) DEFAULT NULL,
  `gender` enum('Male','Female','','') NOT NULL,
  `remarks` varchar(40) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `postby_id` int(40) NOT NULL,
  `updated_at` datetime NOT NULL,
  `updatedby_id` int(11) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `fullname`, `username`, `email`, `contact_no`, `password`, `user_type`, `address`, `gender`, `remarks`, `created_at`, `postby_id`, `updated_at`, `updatedby_id`, `status`) VALUES
(1, 'System', 'admin', NULL, NULL, '21232f297a57a5a743894a0e4a801fc3', '', NULL, 'Female', 'abc', '2017-10-12 14:43:07', 1, '0000-00-00 00:00:00', 0, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `doctor`
--
ALTER TABLE `doctor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patient`
--
ALTER TABLE `patient`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `doctor`
--
ALTER TABLE `doctor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `patient`
--
ALTER TABLE `patient`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
