package pis;

import java.sql.Connection;
import java.sql.DriverManager;
import javax.swing.JOptionPane;

public class MySQLConnect {

    public static Connection conn;

    public static Connection connectDb() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/pis_db", "root", "");
            System.out.println("Database Connected!");
            return conn;

        } catch (Exception e) {
            System.out.println("Error on Database Connection" + e);
            JOptionPane.showMessageDialog(null, "Error on Database Connection");
            System.exit(0);
        }
        return null;
    }

}
