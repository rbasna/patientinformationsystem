package pis;

import java.awt.Toolkit;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class ManagePatient2 extends javax.swing.JFrame {

    Connection conn = null;
    PreparedStatement pst = null;
    ResultSet rs = null;

    public ManagePatient2() {
        initComponents();
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("icon7.jpg")));

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        comboKey = new javax.swing.JComboBox<>();
        txtInputPatient = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        btnSearchPatient = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblPatient = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        btnAddPatient = new javax.swing.JButton();
        btnUpdatePatient = new javax.swing.JButton();
        btnDeletePatient = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        comboKey.setForeground(new java.awt.Color(0, 153, 255));
        comboKey.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select", "ID", "Firstname", "Middlename", "Lastname", "Department", " ", " " }));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 153, 255));
        jLabel1.setText("Manage Patient Section");

        btnSearchPatient.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnSearchPatient.setForeground(new java.awt.Color(0, 153, 255));
        btnSearchPatient.setText("Search");
        btnSearchPatient.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchPatientActionPerformed(evt);
            }
        });

        tblPatient.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tblPatient);

        jPanel1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        btnAddPatient.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btnAddPatient.setForeground(new java.awt.Color(0, 153, 255));
        btnAddPatient.setText("Add");
        btnAddPatient.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddPatientActionPerformed(evt);
            }
        });

        btnUpdatePatient.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btnUpdatePatient.setForeground(new java.awt.Color(0, 153, 255));
        btnUpdatePatient.setText("Update");
        btnUpdatePatient.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdatePatientActionPerformed(evt);
            }
        });

        btnDeletePatient.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btnDeletePatient.setForeground(new java.awt.Color(0, 153, 255));
        btnDeletePatient.setText("Delete");
        btnDeletePatient.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeletePatientActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addComponent(btnAddPatient, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(btnUpdatePatient, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnDeletePatient)
                .addContainerGap(23, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAddPatient, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnUpdatePatient, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnDeletePatient, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(25, Short.MAX_VALUE))
        );

        btnBack.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnBack.setForeground(new java.awt.Color(0, 153, 255));
        btnBack.setText("Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(241, 241, 241)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(239, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(comboKey, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(txtInputPatient, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnSearchPatient)
                        .addGap(23, 23, 23))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(152, 152, 152)
                        .addComponent(btnBack)
                        .addGap(26, 26, 26))))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(comboKey, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtInputPatient, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSearchPatient))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 15, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 315, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnBack, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnSearchPatientActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchPatientActionPerformed
        String key = txtInputPatient.getText();
        String fieldName = comboKey.getSelectedItem().toString();
        System.out.println(fieldName);
        if (key.length() == 0) {
            JOptionPane.showMessageDialog(null, "Enter data to be searched");
        } else {
            String sql = "SELECT * FROM `patient` WHERE " + fieldName + " LIKE '%" + key + "%' AND `status`=1";
            System.out.println(sql);

            try {
                conn = MySQLConnect.connectDb();
                DefaultTableModel table = (DefaultTableModel) tblPatient.getModel();
                table.setRowCount(0);

                conn = MySQLConnect.connectDb();
                //            String sql = "SELECT * FROM `patient` WHERE `status`=1";
                pst = MySQLConnect.conn.prepareStatement(sql);
                System.out.println(sql);
                rs = pst.executeQuery();
                while (rs.next()) {
                    table.addRow(new Object[]{
                        rs.getString("id"),
                        rs.getString("firstname"),
                        rs.getString("middlename"),
                        rs.getString("lastname"),
                        rs.getString("age"),
                        rs.getString("gender"),
                        rs.getString("address"),
                        rs.getString("relative"),
                        rs.getString("contact_no"),
                        rs.getString("height"),
                        rs.getString("weight"),
                        rs.getString("BP"),
                        rs.getString("department"),
                        rs.getString("consultant"), //                        rs.getString("status")
                    });
                }

            } catch (Exception e) {
                System.out.println(e);
                JOptionPane.showMessageDialog(null, e.getMessage());
            }
        }
    }//GEN-LAST:event_btnSearchPatientActionPerformed

    private void btnAddPatientActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddPatientActionPerformed
        AddPatient ad = new AddPatient();
        ad.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnAddPatientActionPerformed

    private void btnUpdatePatientActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdatePatientActionPerformed
        DefaultTableModel table = (DefaultTableModel) tblPatient.getModel();
        int rowCount = tblPatient.getSelectedRowCount();
        if (rowCount != 1) {
            JOptionPane.showMessageDialog(null, "Please select a row to update");
        } else {
            String id = table.getValueAt(tblPatient.getSelectedRow(), 0).toString();
            UpdatePatient2 up = new UpdatePatient2();
            up.LabelID.setText(id);
            this.dispose();
            up.setVisible(true);

        }
    }//GEN-LAST:event_btnUpdatePatientActionPerformed

    private void btnDeletePatientActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeletePatientActionPerformed
        // TODO add your handling code here:
        DefaultTableModel table = (DefaultTableModel) tblPatient.getModel();
        int rowCount = tblPatient.getSelectedRowCount();
        if (rowCount == 0) {
            JOptionPane.showMessageDialog(null, "Please select a row");
        } else {
            String id = table.getValueAt(tblPatient.getSelectedRow(), 0).toString();
            //0-Yes, 1-No ,2-Cancel
            int opt = JOptionPane.showConfirmDialog(null, "Are you sure?");
            if (opt == 0) {
                String sql = "UPDATE `patient` SET `status`=0 WHERE id=" + id;
                try {
                    MySQLConnect.connectDb();
                    pst = MySQLConnect.conn.prepareStatement(sql);
                    pst.execute();

                    refreshTable();//Refresh Jtable
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                    JOptionPane.showMessageDialog(null, "Data cannot be deleted!");
                }
            }
        }
    }//GEN-LAST:event_btnDeletePatientActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed

        HomePage hp = new HomePage();
        hp.setVisible(true);
        dispose();
    }//GEN-LAST:event_btnBackActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        // TODO add your handling code here:
        DefaultTableModel table = (DefaultTableModel) tblPatient.getModel();
        table.addColumn("ID");
        table.addColumn("First Name");
        table.addColumn("Middle Name");
        table.addColumn("Last Name");
        table.addColumn("Age");
        table.addColumn("Gender");
        table.addColumn("Address");
        table.addColumn("Relative");
        table.addColumn("Contact No");
        table.addColumn("Height");
        table.addColumn("Weight(in kg)");
        table.addColumn("Blood Pressure");
        table.addColumn("Department");
        table.addColumn("Consultant");
//        table.addColumn("Status");
        try {
            MySQLConnect.connectDb();
            String sql = "SELECT * FROM `patient` WHERE `status`=1";
            pst = MySQLConnect.conn.prepareStatement(sql);
            System.out.println(sql);
            rs = pst.executeQuery();
            while (rs.next()) {
                table.addRow(new Object[]{
                    rs.getString("id"),
                    rs.getString("firstname"),
                    rs.getString("middlename"),
                    rs.getString("lastname"),
                    rs.getString("age"),
                    rs.getString("gender"),
                    rs.getString("address"),
                    rs.getString("relative"),
                    rs.getString("contact_no"),
                    rs.getString("height"),
                    rs.getString("weight"),
                    rs.getString("BP"),
                    rs.getString("department"),
                    rs.getString("consultant") //                    rs.getString("status")
                });
            }

        } catch (Exception e) {
            System.out.println(e);
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }//GEN-LAST:event_formWindowOpened

    private void refreshTable() {
        try {
            DefaultTableModel table = (DefaultTableModel) tblPatient.getModel();
            table.setRowCount(0);

            conn = MySQLConnect.connectDb();
            String sql = "SELECT * FROM `patient` WHERE `status`=1";
            pst = MySQLConnect.conn.prepareStatement(sql);
            System.out.println(sql);
            rs = pst.executeQuery();
            while (rs.next()) {
                table.addRow(new Object[]{
                    rs.getString("id"),
                    rs.getString("firstname"),
                    rs.getString("middlename"),
                    rs.getString("lastname"),
                    rs.getString("age"),
                    rs.getString("gender"),
                    rs.getString("address"),
                    rs.getString("relative"),
                    rs.getString("contact_no"),
                    rs.getString("height"),
                    rs.getString("weight"),
                    rs.getString("BP"),
                    rs.getString("department"),
                    rs.getString("consultant"), //                    rs.getString("status")
                });
            }

        } catch (Exception e) {
            System.out.println(e);
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ManagePatient2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ManagePatient2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ManagePatient2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ManagePatient2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ManagePatient2().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddPatient;
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnDeletePatient;
    private javax.swing.JButton btnSearchPatient;
    private javax.swing.JButton btnUpdatePatient;
    private javax.swing.JComboBox<String> comboKey;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblPatient;
    private javax.swing.JTextField txtInputPatient;
    // End of variables declaration//GEN-END:variables
}
