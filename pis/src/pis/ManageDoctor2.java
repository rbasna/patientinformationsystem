package pis;

import java.awt.Toolkit;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class ManageDoctor2 extends javax.swing.JFrame {

    Connection conn = null;
    PreparedStatement pst = null;
    ResultSet rs = null;

    public ManageDoctor2() {
        initComponents();
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("icon7.jpg")));

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtInputDoctor = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        comboKey = new javax.swing.JComboBox<>();
        btnSearchDoctor = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblDoctor = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        btnAddDoctor1 = new javax.swing.JButton();
        btnUpdateDoctor1 = new javax.swing.JButton();
        btnDeleteDoctor1 = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 153, 255));
        jLabel1.setText("Manage Doctor Section");

        comboKey.setForeground(new java.awt.Color(0, 153, 255));
        comboKey.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select", "ID", "Firstname", "Middlename", "Lastname", "Department" }));

        btnSearchDoctor.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnSearchDoctor.setForeground(new java.awt.Color(0, 153, 255));
        btnSearchDoctor.setText("Search");
        btnSearchDoctor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchDoctorActionPerformed(evt);
            }
        });

        tblDoctor.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tblDoctor);

        jPanel2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        btnAddDoctor1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btnAddDoctor1.setForeground(new java.awt.Color(0, 153, 255));
        btnAddDoctor1.setText("Add");
        btnAddDoctor1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddDoctor1ActionPerformed(evt);
            }
        });

        btnUpdateDoctor1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btnUpdateDoctor1.setForeground(new java.awt.Color(0, 153, 255));
        btnUpdateDoctor1.setText("Update");
        btnUpdateDoctor1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateDoctor1ActionPerformed(evt);
            }
        });

        btnDeleteDoctor1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btnDeleteDoctor1.setForeground(new java.awt.Color(0, 153, 255));
        btnDeleteDoctor1.setText("Delete");
        btnDeleteDoctor1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteDoctor1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(48, 48, 48)
                .addComponent(btnAddDoctor1, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(38, 38, 38)
                .addComponent(btnUpdateDoctor1)
                .addGap(23, 23, 23)
                .addComponent(btnDeleteDoctor1, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(27, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAddDoctor1, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnUpdateDoctor1, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnDeleteDoctor1, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(27, Short.MAX_VALUE))
        );

        btnBack.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnBack.setForeground(new java.awt.Color(0, 153, 255));
        btnBack.setText("Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(comboKey, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtInputDoctor, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnSearchDoctor)
                .addGap(30, 30, 30))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(164, 164, 164)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 200, Short.MAX_VALUE)
                .addComponent(btnBack)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(239, 239, 239)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtInputDoctor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSearchDoctor)
                    .addComponent(comboKey, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 330, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnBack)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSearchDoctorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchDoctorActionPerformed

        String key = txtInputDoctor.getText();
        String fieldName = comboKey.getSelectedItem().toString();
        System.out.println(fieldName);
        if (key.length() == 0) {
            JOptionPane.showMessageDialog(null, "Enter data to be searched");
        } else {
            String sql = "SELECT * FROM `doctor` WHERE " + fieldName + " LIKE '%" + key + "%' AND `status`=1";
            System.out.println(sql);

            try {
                conn = MySQLConnect.connectDb();
                DefaultTableModel table = (DefaultTableModel) tblDoctor.getModel();
                table.setRowCount(0);

                conn = MySQLConnect.connectDb();

                pst = MySQLConnect.conn.prepareStatement(sql);
                System.out.println(sql);
                rs = pst.executeQuery();
                while (rs.next()) {
                    table.addRow(new Object[]{
                        rs.getString("id"),
                        rs.getString("firstname"),
                        rs.getString("middlename"),
                        rs.getString("lastname"),
                        rs.getString("username"),
                        rs.getString("password"),
                        rs.getString("age"),
                        rs.getString("gender"),
                        rs.getString("address"),
                        rs.getString("qualification"),
                        rs.getString("contact_no"),
                        rs.getString("department"),
                        rs.getString("joined_date") //                        rs.getString("status")
                    });
                }

            } catch (Exception e) {
                System.out.println(e);
                JOptionPane.showMessageDialog(null, e.getMessage());
            }
        }
    }//GEN-LAST:event_btnSearchDoctorActionPerformed

    private void btnAddDoctor1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddDoctor1ActionPerformed
        AddDoctor2 ad = new AddDoctor2();
        ad.setVisible(true);
    }//GEN-LAST:event_btnAddDoctor1ActionPerformed

    private void btnUpdateDoctor1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateDoctor1ActionPerformed

        DefaultTableModel table = (DefaultTableModel) tblDoctor.getModel();
        int rowCount = tblDoctor.getSelectedRowCount();
        if (rowCount != 1) {
            JOptionPane.showMessageDialog(null, "Please select a row to update");
        } else {
            String id = table.getValueAt(tblDoctor.getSelectedRow(), 0).toString();
            UpdateDoctor2 u = new UpdateDoctor2();
            u.LabelID.setText(id);
            this.dispose();
            u.setVisible(true);

        }
    }//GEN-LAST:event_btnUpdateDoctor1ActionPerformed

    private void btnDeleteDoctor1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteDoctor1ActionPerformed
        // TODO add your handling code here:
        DefaultTableModel table = (DefaultTableModel) tblDoctor.getModel();
        int rowCount = tblDoctor.getSelectedRowCount();
        if (rowCount != 1) {
            JOptionPane.showMessageDialog(null, "Please select a row");
        } else {
            String id = table.getValueAt(tblDoctor.getSelectedRow(), 0).toString();
            //0-Yes, 1-No ,2-Cancel
            int opt = JOptionPane.showConfirmDialog(null, "Are you sure?");
            if (opt == 0) {
                String sql = "UPDATE `doctor` SET `status`=0 WHERE id=" + id;
                try {
                    MySQLConnect.connectDb();
                    pst = MySQLConnect.conn.prepareStatement(sql);
                    pst.execute();

                    refreshTable();//Refresh Jtable
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                    JOptionPane.showMessageDialog(null, "Data cannot be deleted!");
                }
            }
        }
    }//GEN-LAST:event_btnDeleteDoctor1ActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        // TODO add your handling code here:
        HomePage hp = new HomePage();
        hp.setVisible(true);
        dispose();
    }//GEN-LAST:event_btnBackActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        // TODO add your handling code here:
        DefaultTableModel table = (DefaultTableModel) tblDoctor.getModel();
        table.addColumn("ID");
        table.addColumn("First Name");
        table.addColumn("Middle Name");
        table.addColumn("Last Name");
        table.addColumn("Username");
        table.addColumn("Password");
        table.addColumn("Age");
        table.addColumn("Gender");
        table.addColumn("Address");
        table.addColumn("Qualification");
        table.addColumn("Contact No");
        table.addColumn("Department");
        table.addColumn("Joined Date");
//        table.addColumn("Status");
        try {
            MySQLConnect.connectDb();
            String sql = "SELECT * FROM `doctor` WHERE `status`=1";
            pst = MySQLConnect.conn.prepareStatement(sql);
            System.out.println(sql);
            rs = pst.executeQuery();
            while (rs.next()) {
                table.addRow(new Object[]{
                    rs.getString("id"),
                    rs.getString("firstname"),
                    rs.getString("middlename"),
                    rs.getString("lastname"),
                    rs.getString("username"),
                    rs.getString("password"),
                    rs.getString("age"),
                    rs.getString("gender"),
                    rs.getString("address"),
                    rs.getString("qualification"),
                    rs.getString("contact_no"),
                    rs.getString("department"),
                    rs.getString("joined_date") //                    rs.getString("status")
                });
            }

        } catch (Exception e) {
            System.out.println(e);
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }//GEN-LAST:event_formWindowOpened

    private void refreshTable() {
        try {
            DefaultTableModel table = (DefaultTableModel) tblDoctor.getModel();
            table.setRowCount(0);

            conn = MySQLConnect.connectDb();
            String sql = "SELECT * FROM `doctor` WHERE `status`=1";
            pst = MySQLConnect.conn.prepareStatement(sql);
            System.out.println(sql);
            rs = pst.executeQuery();
            while (rs.next()) {
                table.addRow(new Object[]{
                    rs.getString("id"),
                    rs.getString("firstname"),
                    rs.getString("middlename"),
                    rs.getString("lastname"),
                    rs.getString("username"),
                    rs.getString("password"),
                    rs.getString("age"),
                    rs.getString("gender"),
                    rs.getString("address"),
                    rs.getString("qualification"),
                    rs.getString("contact_no"),
                    rs.getString("department"),
                    rs.getString("joined_date")
//                    rs.getString("status")
                });
            }

        } catch (Exception e) {
            System.out.println(e);
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ManageDoctor2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ManageDoctor2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ManageDoctor2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ManageDoctor2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ManageDoctor2().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddDoctor;
    private javax.swing.JButton btnAddDoctor1;
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnDeleteDoctor;
    private javax.swing.JButton btnDeleteDoctor1;
    private javax.swing.JButton btnSearchDoctor;
    private javax.swing.JButton btnUpdateDoctor;
    private javax.swing.JButton btnUpdateDoctor1;
    private javax.swing.JComboBox<String> comboKey;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblDoctor;
    private javax.swing.JTextField txtInputDoctor;
    // End of variables declaration//GEN-END:variables
}
